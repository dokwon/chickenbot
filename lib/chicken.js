'use strict';

var util = require('util');
var path = require('path');
var fs = require('fs');
var SQLite = require('sqlite3').verbose();
var Bot = require('slackbots');


var ChickenBot = function Constructor(settings) {
    this.settings = settings;
    this.settings.name = this.settings.name || 'menu';
    this.dbPath = settings.dbPath || path.resolve(process.cwd(), 'data', 'chicken.db');

    this.user = null;
    this.db = null;

    var self = this; 
    // Set timer to fire
    setInterval(function() {
        // Heroku server is hosted in GMT, and Korea time is GMT + 9
        // TODO: normalize this to localize this to korea time irrespective of where heroku is hosted.
  		var currentTime = new Date(new Date().valueOf() + 32400000);

        // Fetch time information beforehand
        var day = currentTime.getDay();
        var hour = currentTime.getHours();
        var minute = currentTime.getMinutes();

        // Bot runs from Monday through Friday
  		if (!day || day > 5) {
            return;
        }

  		// Ask for lunch options
  		if (hour === 10 && minute === 10) {
  			self._writeToEveryUser("꼬꼬댁!! 오늘도 점심 먹어야 코드짜지 않겠나... 뭐먹을래?");
  		}

  		// Notify lunch options
  		if (hour === 12 && minute === 30) {
  			self._retrieveMenu();
  		}

  		// Ask for dinner options
  		if (hour === 16 && minute === 10) {
  			self._writeToEveryUser("꼬꼬댁!! 저녁 뭐먹을지 후딱말해 나두 퇴근하게~~ ... 뭐먹을래? ");
  		}

  		// Notify dinner options
  		if (hour === 18 && minute === 30) {
  			self._retrieveMenu();
  		}
	}, 
	// fire every minute
	60 * 1000);
};

// inherits methods and properties from the Bot constructor
util.inherits(ChickenBot, Bot);

module.exports = ChickenBot;

ChickenBot.prototype.run = function () {
    ChickenBot.super_.call(this, this.settings);

    this.on('start', this._onStart);
    this.on('message', this._onMessage);
};

ChickenBot.prototype._onStart = function () {
    this._loadBotUser();
    this._connectDb();
    this._firstRunCheck();
};

ChickenBot.prototype._loadBotUser = function () {
    var self = this;
    this.user = this.users.filter(function (user) {
        return user.name === self.name;
    })[0];
};

ChickenBot.prototype._connectDb = function () {
    if (!fs.existsSync(this.dbPath)) {
        console.error('Database path ' + '"' + this.dbPath + '" does not exists or it\'s not readable.');
        process.exit(1);
    }

    this.db = new SQLite.Database(this.dbPath);
};


ChickenBot.prototype._firstRunCheck = function () {
    var self = this;
    self.db.get('SELECT val FROM info WHERE name = "lastrun" LIMIT 1', function (err, record) {
        if (err) {
            return console.error('DATABASE ERROR:', err);
        }

        var currentTime = (new Date()).toJSON();

        // this is a first run
        if (!record) {
            self._welcomeMessage();
            return self.db.run('INSERT INTO info(name, val) VALUES("lastrun", ?)', currentTime);
        }

        // updates with new last running time
        self.db.run('UPDATE info SET val = ? WHERE name = "lastrun"', currentTime);
    });
};

ChickenBot.prototype._welcomeMessage = function () {
    this.postMessageToChannel(this.channels[0].name, '뭐먹고싶은지 빨랑말해!! ' +
         + this.name + '라고 부르면 돼!',
        {as_user: true});
};

ChickenBot.prototype._randomReject = function() {
	var seed = Math.floor((Math.random() * 10) + 1);
    return seed >= 8;
}

ChickenBot.prototype._isChickenDish = function(menuchoice) {
	var mc = menuchoice.toLowerCase();
    var chickendish = ["치킨", "삼계탕", "닭", "chicken"];
    for (var i = 0; i < chickendish.length; i ++) {
        if (mc.indexOf(chickendish[i]) !== -1) return true;
    }
    return false;
}

ChickenBot.prototype._writeToEveryUser = function(message) {

	for (var i = 0; i < this.users.length; i ++) {
		var user = this.users[i];
		this.postMessageToUser(user.name, message, {as_user: true});
	}
}

ChickenBot.prototype._respondToMessage = function(channel, user, resultMsg) {
	if (resultMsg != undefined) {
		if (channel != null) {
			this.postMessageToChannel(channel.name, resultMsg, {as_user: true});
	   	} else if (user != null) {
	 		this.postMessageToUser(user.name, resultMsg, {as_user: true});
	    }
	}
};

ChickenBot.prototype._onMessage = function (message) {
    if (this._isChatMessage(message) &&
        !this._isFromChickenBot(message)
    ) {
    	var channel = this._getChannelById(message.channel);
    	var user = this._getUserById(message.user);

    	if (channel != null && !this._mentionsChickenBot(message)) {
    		return;
    	}
   			
   		var resultMsg = undefined;
   		if (this._isClearRequest(message)) {
   			this._clearMenu();
   			resultMsg = "As you command. The menu has been cleared. ";
   		} else if (this._isResultRequest(message)) {
    		this._retrieveMenu();
    		return;
    	} else {
    		var menuchoice = message.text.replace("@" + this.user.id);
    		var self = this;
			this._retrieveUserChoices(message.user, function(entryCount) {
    			if (entryCount !== undefined && entryCount > 3) {
    				resultMsg = "마!! 그만 올려라. 확 밥 안줘부릴까부다 ~~";
    			} else if (self._randomReject()) {
    				resultMsg = "뭘 그런걸먹어!! 딴거골라 후딱~";
    			} else {
    				if (self._isChickenDish(menuchoice)) {
    					resultMsg = "ㅎㄷㄷ..오키댁...알아들었음....ㅠ.ㅠ";
    				} else {
    					resultMsg = "오키댁 알아들었음 ^:>~";
    				}
    				self._saveMenu(message);	
    			}
    			self._respondToMessage(channel, user, resultMsg);
    		});
    	}

    	this._respondToMessage(channel, user, resultMsg);
    }
};

ChickenBot.prototype._isClearRequest = function (message) {
    return  message.text.indexOf("obsidian") > -1; 
};

ChickenBot.prototype._isChatMessage = function (message) {
    return message.type == 'message' && Boolean(message.text);
};

ChickenBot.prototype._mentionsChickenBot = function (message) {
    return message.text.indexOf(this.user.id) > -1; 
};

ChickenBot.prototype._isFromChickenBot = function (message) {
    return message.user == this.user.id;
};

ChickenBot.prototype._isResultRequest = function (message) {
    return message.text.toLowerCase().indexOf('?') > -1 ||
        message.text.toLowerCase().indexOf("결과") > -1 || 
        message.text.toLowerCase().indexOf("메뉴") > -1 || 
        message.text.toLowerCase().indexOf("result") > -1;
};

ChickenBot.prototype._saveMenu = function (message) {
    var self = this;
    self.db.get('INSERT INTO choices (username, menu) VALUES (\'' + message.user + '\' ,\'' 
    	+ message.text + '\')' , function (err, record) {
        if (err) {
            return console.error('save DATABASE ERROR:', err);
        }
    });
};

ChickenBot.prototype._retrieveUserChoices = function (userid, callback) {
    var self = this;
	self.db.get('select count(*) from Choices WHERE username = ' + "\'" + userid + "\'", function (err, record) {
	    if (err) {
	        return console.error('userchoice DATABASE ERROR:', err);
	    }

	    callback(record['count(*)']);
	});
};

ChickenBot.prototype._retrieveMenu = function () {
    var self = this;
	self.db.get('SELECT username, menu FROM Choices ORDER BY RANDOM() LIMIT 1', function (err, record) {
	    if (err) {
	        return console.error('retrieve DATABASE ERROR:', err);
	    }

	    var resultMsg = "";
	    if (record != null) {
	        resultMsg = "꼬꼬댁!! 오늘 너희가 먹을 메뉴다. Menu : " + record.menu + 
	        			" winner: " + self._getUserById(record.username).name;
	    } else {
	        resultMsg = "에휴 게으른것들아... Menu : 부대찌개 winner: Do";
	    }

		self.postMessageToChannel("general", resultMsg, {as_user: true});

	    self._clearMenu();
	});
};

ChickenBot.prototype._clearMenu = function () {
    var self = this;
    self.db.get('DELETE FROM Choices' , function (err, record) {
        if (err) {
            return console.error('clear DATABASE ERROR:', err);
        }
    });
};

ChickenBot.prototype._getChannelById = function (channelId) {
    return this.channels.filter(function (item) {
        return item.id === channelId;
    })[0];
};

ChickenBot.prototype._getUserById = function (userId) {
    return this.users.filter(function (item) {
        return item.id === userId;
    })[0];
};

