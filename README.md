

### What is this repository for? ###

본 리파지토리는 색깔있게 메뉴를 통보해주는 치킨봇의 서식지입니다. 꼬꼬댁~ 

product enhancement 는 치킨봇 jira 페이지에 추가해주시고, 창의적인 기능은 언제든지 DM 이나 PR 로 던져주시기 바랍니다. 

### How do I get set up? ###

그냥 풀 하시고...

루트 디렉토리에서 node bin/bot.js 실행하시면 됩니다. 

개발환경에 노드가 설치되있지 않다면 npm/ sqlite3 / node 를 설치해주시기 바랍니다. 

### Contribution guidelines ###

평소에 하던대로 PULL REQUEST 작성하고, 관리자가 MERGE 할때까지 대기해주시기 바랍니다. 

### Who do I talk to? ###

질문사항은 do@anyfi.io 로 해주시기 바랍니다. 